<!DOCTYPE html>
<html>

<head>
<title>DEMOSTRATION</title>
</head>

<style>

	body            	{ background-color: red; color: white; }	
	table.db-table 		{ align=center; border-right:1px solid #ccc; border-bottom:1px solid #ccc; }
	table.db-table th	{ align=center; background:#404040; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
	table.db-table td	{ align=center; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
	.even th, .even td  { background: blue; }

</style>

<body>

<table>

	
	<?php
	
	$con = mysqli_connect("localhost","root","","mydb");
	
	// ********* Check Connection **********
	if (mysqli_connect_errno())
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	else
	{
		echo "success!<br>";
	}
		
	// ********** Insert Data **********
	$sql="INSERT INTO table1 (Name)
	VALUES
	('$_POST[name]')";

	if (!mysqli_query($con,$sql))
	{
		die('Error: ' . mysqli_error($con));
	}
		
	// ********** Delete Data **********
	$dsql=" DELETE FROM table1 WHERE id='$_POST[id]' ";
	
	if (!mysqli_query($con,$dsql))
	{
		die('Error: ' . mysqli_error($con));
	}
		
	?>
    
	
	
	<?php
	
    // ********** Show Table **********
	$result = mysqli_query($con,"SELECT * FROM table1");
	
	if(mysqli_num_rows($result)) 
	{
		echo '<table cellpadding="0" cellspacing="0" class="db-table">';
		echo '<tr><th>id</th><th>Name</th></tr>';
		
		$num_row = 0;		
		while($row = mysqli_fetch_array($result))
		{
		    if($num_row % 2 == 0 )
			{
				echo '<tr class="even">';	
				for($x=0; $x<2; $x++)
				{
					echo '<td>',$row[$x],'</td>';
				}
				echo '</tr>';
			}
			else
			{
				echo '<tr class="odd">';	
				for($x=0; $x<2; $x++)
				{
					echo '<td>',$row[$x],'</td>';
				}
				echo '</tr>';
			}
			$num_row++;
		}
			
		echo '</table><br />';
    }
		
	mysqli_close($con);
	
	?> 
		

</table>

</body>

</html>